# all: test
all: build

test: prepare
	latexmk -pdf -jobname=tmp/test \
	-xelatex \
	-synctex=1 test.tex

build: prepare
	latexmk -pdf -jobname=tmp/thesis \
	-xelatex \
	-synctex=1 main.tex

whole: 1 2

1: prepare
	latexmk -pdf -jobname=tmp/thesis \
	-xelatex \
	-synctex=1 inc/1-literature.tex

2: prepare
	latexmk -pdf -jobname=tmp/thesis \
	-xelatex \
	-synctex=1 inc/2-special.tex


.PHONY: showall
showall: display example


.PHONY: display
display: build
	@evince tmp/thesis.pdf > /dev/null 2>&1 &

example:
	@evince ex/diploma.pdf > /dev/null 2>&1 &

.PHONY: display_test
display_test: test
	@evince tmp/test.pdf > /dev/null 2>&1 &

.PHONY: clean
clean:
	@rm -rf tmp

prepare:
	@mkdir -p tmp