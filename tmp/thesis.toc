\select@language {russian}
\contentsline {section}{\numberline {1}Аналитический обзор литературы}{6}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Модель камеры-обскуры}{7}{subsection.1.1}% 
\contentsline {subsubsection}{\numberline {1.1.1}Внутренние параметры камеры}{8}{subsubsection.1.1.1}% 
\contentsline {subsubsection}{\numberline {1.1.2}Перспективная проекция с использованием однородных координат}{8}{subsubsection.1.1.2}% 
\contentsline {subsubsection}{\numberline {1.1.3}Смещение главной точки}{9}{subsubsection.1.1.3}% 
\contentsline {subsubsection}{\numberline {1.1.4}Характеристики датчика изображения}{10}{subsubsection.1.1.4}% 
\contentsline {subsubsection}{\numberline {1.1.5}Искажение радиальной линзы}{11}{subsubsection.1.1.5}% 
\contentsline {subsection}{\numberline {1.2}Внешние параметры камеры}{15}{subsection.1.2}% 
\contentsline {subsubsection}{\numberline {1.2.1}Обратная проекция 2D точки в 3D}{16}{subsubsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.3}Преобразование системы координат}{17}{subsection.1.3}% 
\contentsline {subsubsection}{\numberline {1.3.1}Изменение системы координат изображения}{17}{subsubsection.1.3.1}% 
\contentsline {subsubsection}{\numberline {1.3.2}Изменение мировой системы координат}{18}{subsubsection.1.3.2}% 
\contentsline {subsection}{\numberline {1.4}Калибровка камеры}{19}{subsection.1.4}% 
\contentsline {subsubsection}{\numberline {1.4.1}Оценка параметров камеры}{20}{subsubsection.1.4.1}% 
\contentsline {subsubsection}{\numberline {1.4.2}Оценка гомографического преобразования}{21}{subsubsection.1.4.2}% 
\contentsline {subsubsection}{\numberline {1.4.3}Расчет внутренних параметров}{24}{subsubsection.1.4.3}% 
\contentsline {subsubsection}{\numberline {1.4.4}Расчет внешних параметров}{26}{subsubsection.1.4.4}% 
\contentsline {subsubsection}{\numberline {1.4.5}Нелинейное уточнение параметров камеры}{27}{subsubsection.1.4.5}% 
\contentsline {subsection}{\numberline {1.5}Двухракурсная геометрия}{27}{subsection.1.5}% 
\contentsline {subsubsection}{\numberline {1.5.1}Эпиполярная геометрия}{28}{subsubsection.1.5.1}% 
\contentsline {subsubsection}{\numberline {1.5.2}Исправление изображения}{30}{subsubsection.1.5.2}% 
\contentsline {subsubsection}{\numberline {1.5.3}Расчет матрицы $ R '$}{32}{subsubsection.1.5.3}% 
\contentsline {subsubsection}{\numberline {1.5.4}Расчет матрицы $ K '$}{33}{subsubsection.1.5.4}% 
\contentsline {subsubsection}{\numberline {1.5.5}Расчет ограничительной рамки}{33}{subsubsection.1.5.5}% 
\contentsline {subsubsection}{\numberline {1.5.6}Краткое изложение алгоритма}{34}{subsubsection.1.5.6}% 
\contentsline {subsection}{\numberline {1.6}Нахождение особых точек}{34}{subsection.1.6}% 
\contentsline {section}{\numberline {2}Специальная часть}{39}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Постановка задачи и функциональная схема системы}{39}{subsection.2.1}% 
\contentsline {subsubsection}{\numberline {2.1.1}Содержательная постановка задачи}{39}{subsubsection.2.1.1}% 
\contentsline {subsubsection}{\numberline {2.1.2}Математическая постановка задачи}{41}{subsubsection.2.1.2}% 
\contentsline {subsubsection}{\numberline {2.1.3}Функциональная схема системы}{42}{subsubsection.2.1.3}% 
\contentsline {subsection}{\numberline {2.2}Алгоритм восстановления структуры объекта по данным из видеопотока}{44}{subsection.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.1}Поиск соответсвий}{44}{subsubsection.2.2.1}% 
\contentsline {subsubsection}{\numberline {2.2.2}Извлечение признаков.}{45}{subsubsection.2.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.3}Сопоставление.}{45}{subsubsection.2.2.3}% 
\contentsline {subsubsection}{\numberline {2.2.4}Геометрическая проверка.}{47}{subsubsection.2.2.4}% 
\contentsline {subsubsection}{\numberline {2.2.5}Инкрементальная реконструкция}{47}{subsubsection.2.2.5}% 
\contentsline {subsubsection}{\numberline {2.2.6}Инициализация реконструкции.}{48}{subsubsection.2.2.6}% 
\contentsline {subsubsection}{\numberline {2.2.7}Регистрация изображений.}{49}{subsubsection.2.2.7}% 
\contentsline {subsubsection}{\numberline {2.2.8}Триангуляция.}{51}{subsubsection.2.2.8}% 
\contentsline {subsubsection}{\numberline {2.2.9}Групповая корректировка.}{52}{subsubsection.2.2.9}% 
